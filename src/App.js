import React from 'react';

import Header from './components/Header'
import Navigation from './components/Navigation'
import Banner from './components/Banner'
import One from './components/One'
import Two from './components/Two'
import Contact from './components/Contact'
import Footer from './components/Footer'

function App() {
  return (
    <div id="wrapper">
      <Header />
      <Navigation />
      <Banner />

      <div id="main">
        <One />
        <Two />
      </div>

      <Contact />
      <Footer />

    </div>

  );
}

export default App;
