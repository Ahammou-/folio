import React from 'react'

export default function Navigation() {
    return (
        <nav id="menu">
          <div className="links">
            <li><a href="index.html">Home</a></li>
            <li><a href="landing.html">Landing</a></li>
            <li><a href="generic.html">Generic</a></li>
            <li><a href="elements.html">Elements</a></li>
          </div>
          <ul className="actions stacked">
            <li><a href="#0" className="button primary fit">Get Started</a></li>
            <li><a href="#0" className="button fit">Log In</a></li>
          </ul>
        </nav>
    )
}