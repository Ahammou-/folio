import React from 'react'

export default function Header() {
    return (
        <header id="header" className="alt">
            <a href="index.html" className="logo"><strong>AHA</strong><span>mmou-</span></a>
            <nav>
                <a href="#menu">Menu</a>
            </nav>
        </header>
    )
}